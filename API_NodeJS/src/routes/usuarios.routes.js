const {Router} = require("express")
const ctrUsu = require("../controllers/usuario.controller")
const routerUsuario = Router()


// Ruta para la creación de un producto
routerUsuario.post('/', ctrUsu.agregarUsuario)

// Ruta para obtener todos los productos registrados
routerUsuario.get('/', ctrUsu.obtenerUsuario)

// Ruta para obtener un producto en particular
routerUsuario.get('/:nombre', ctrUsu.obtenerunUsuario)

// Ruta para actualizar un producto completo
routerUsuario.put('/:id', ctrUsu.actualizarUsuario)

// Ruta para borrar un producto en particular
routerUsuario.delete('/:id', ctrUsu.eliminarUsuario)

module.exports = routerUsuario