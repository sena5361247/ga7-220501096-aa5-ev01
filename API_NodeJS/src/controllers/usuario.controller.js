const Usuario = require("../models/Usuarios")



exports.obtenerUsuario= async (req, res) => {
try {
  const usuarios = await Usuario.find()
  res.json(usuarios)
} catch (error) {
  res.json(error)  
}
  }

exports.obtenerunUsuario= async (req, res) => {
  try {
    const {nombre,correo} = req.body

    if (nombre && correo){      
      const usuarios = await Usuario.findOne({nombre},{correo}).exec()
      if (!usuarios) {
        res.json({msj:"Error en la Autenticacion"})     
        
      } else {
        
        res.json({msj:"Autenticacion Satisfactoria"})  
      }       
    
    } else {
      res.json({isOk: false, msj: "Se requieren datos"})
    }
    
  } catch (error) {
  res.json(error)
    
  }
    }

exports.agregarUsuario = async (req, res) => {
  try {
    const {nombre, apellido,cedula,correo} = req.body
    console.log(nombre)

    if (nombre && apellido && cedula && correo){
      const nuevoUsuario = new Usuario({nombre, apellido,cedula,correo})
      await nuevoUsuario.save()
      res.json({msj:"documento insertado satisfactoriamente"})
    } else {
      res.json({isOk: false, msj: "Los datos son requeridos"})
    }
    
  } catch (error) {
    res.json(error)   
  }
  }

exports.actualizarUsuario = async (req, res) => {
try {
  const id = req.params.id
  const data = req.body
  

  if (id && data){
    await Usuario.findByIdAndUpdate(id, data)    
    res.json("Registro actualizado")    
  } else {
    res.json({msj:"Registro no actualizado"})     
  }
} catch (error) {
  res.json(error)
}
  }

    
exports.eliminarUsuario = async (req, res) => {
try {
  const id = req.params.id
  console.log(id)
  const eliminado = await Usuario.findByIdAndDelete(id)
  res.status(200).json({msj:"id recibida para eliminiar usuario", isOk: true})
} catch (error) {
  res.status(500).json(error)
}
  }


